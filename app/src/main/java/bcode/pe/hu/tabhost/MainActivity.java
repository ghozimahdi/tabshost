package bcode.pe.hu.tabhost;

import android.app.TabActivity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TabHost;


@SuppressWarnings("deprecation")
public class MainActivity extends TabActivity {

    private TabHost tabHost;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupView();
    }

    private void setupView() {
        tabHost = (TabHost) findViewById(android.R.id.tabhost);

        TabHost.TabSpec tabmenu1  = tabHost.newTabSpec("first tab");
        TabHost.TabSpec tabmenu2  = tabHost.newTabSpec("second tab");
        TabHost.TabSpec tabmenu3  = tabHost.newTabSpec("third tab");

        tabmenu1.setIndicator("",getResources().getDrawable(R.drawable.ic_assignment_return_black_24dp));
        tabmenu1.setContent(new Intent(MainActivity.this,Activity_satu.class));

        tabmenu2.setIndicator("tab 2");
        tabmenu2.setContent(new Intent(MainActivity.this,Activity_dua.class));

        tabmenu3.setIndicator("tab 3");
        tabmenu3.setContent(new Intent(MainActivity.this,Activity_tiga.class));

        tabHost.addTab(tabmenu1);
        tabHost.addTab(tabmenu2);
        tabHost.addTab(tabmenu3);
    }
}
